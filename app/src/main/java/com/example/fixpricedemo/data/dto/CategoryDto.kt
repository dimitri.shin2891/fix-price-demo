package com.example.fixpricedemo.data.dto

import com.google.gson.annotations.SerializedName

data class CategoryItemDto(
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val title: String?,
    @SerializedName("catalogImage")
    val catalogImage: String?
)