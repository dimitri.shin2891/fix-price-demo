package com.example.fixpricedemo.data.repositories

import com.example.fixpricedemo.data.Api
import com.example.fixpricedemo.data.mappers.CategoryMapper
import com.example.fixpricedemo.domen.model.Category
import com.example.fixpricedemo.domen.repositories.CategoryRepository

class CategoryRepositoryImpl(
    private val api: Api,
    private val categoryMapper: CategoryMapper) : CategoryRepository {

    override suspend fun getCategory(): List<Category> {
        return categoryMapper.mapToCategory(api.getCategory())
    }
}