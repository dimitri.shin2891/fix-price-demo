package com.example.fixpricedemo.data.mappers

import com.example.fixpricedemo.data.dto.CategoryItemDto
import com.example.fixpricedemo.domen.model.Category

class CategoryMapper {

    fun mapToCategory(categoryItemDtoList: List<CategoryItemDto>?): List<Category> {
        return categoryItemDtoList
            ?.map { categoryItemDto ->
                Category(
                    id = categoryItemDto.id,
                    title = categoryItemDto.title ?: "",
                    catalogImage = categoryItemDto.catalogImage ?: ""
                )
            } ?: emptyList()
    }
}