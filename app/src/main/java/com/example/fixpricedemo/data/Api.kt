package com.example.fixpricedemo.data

import com.example.fixpricedemo.data.dto.CategoryItemDto
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    // магическое число 40. Захардкодил cityid, так как не реализовывал функционал выбора города.
    @GET("/buyer/v2/category")
    suspend fun getCategory(@Query("cityid") cityId: Int = 40): List<CategoryItemDto>?
}