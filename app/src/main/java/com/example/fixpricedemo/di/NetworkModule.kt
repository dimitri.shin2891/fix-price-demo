package com.example.fixpricedemo.di

import com.example.fixpricedemo.data.Api
import com.example.fixpricedemo.utils.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.BuildConfig
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

val netWorkModule = module {

    factory {
        provideLoggingInterceptor()
    }

    factory {
        provideOkHttpClient(
            networkInterceptor = get(),
            headerInterceptor = get()
        )
    }

    factory {
        provideApi(okHttpClient = get(), gsonConverterFactory = get())
    }

    factory {
        provideGson()
    }

    factory { provideGsonConverterFactory(gson = get()) }

    factory { HeaderInterceptor() }
}

private fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()

    val loggingLevel: HttpLoggingInterceptor.Level = if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor.Level.BODY
    } else {
        HttpLoggingInterceptor.Level.NONE
    }

    logger.level = loggingLevel
    return logger
}

private fun provideOkHttpClient(
    networkInterceptor: HttpLoggingInterceptor,
    headerInterceptor: HeaderInterceptor
): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(headerInterceptor)
        .addNetworkInterceptor(networkInterceptor)
        .build()
}

private fun provideGson(): Gson = GsonBuilder().create()

private fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory {
    return GsonConverterFactory.create(gson)
}

private fun provideApi(
    okHttpClient: OkHttpClient,
    gsonConverterFactory: GsonConverterFactory
): Api {
    return Retrofit.Builder()
        .baseUrl(Constants.API_URL)
        .client(okHttpClient)
        .addConverterFactory(gsonConverterFactory)
        .build().create()
}

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("User-Agent", "BUYER-FRONT-ANDROID 3.39")
                .build()
        )
    }
}