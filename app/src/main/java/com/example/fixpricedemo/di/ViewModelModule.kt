package com.example.fixpricedemo.di

import androidx.lifecycle.SavedStateHandle
import com.example.fixpricedemo.ui.screens.CategoryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { (handle: SavedStateHandle) -> CategoryViewModel(handle, categoryRepository = get()) }
}