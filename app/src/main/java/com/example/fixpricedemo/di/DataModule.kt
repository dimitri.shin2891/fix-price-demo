package com.example.fixpricedemo.di

import com.example.fixpricedemo.data.mappers.CategoryMapper
import com.example.fixpricedemo.data.repositories.CategoryRepositoryImpl
import com.example.fixpricedemo.domen.repositories.CategoryRepository
import org.koin.dsl.module

val dataModule = module {

    single<CategoryRepository> { CategoryRepositoryImpl(api = get(), categoryMapper = get()) }

    factory { CategoryMapper() }
}