package com.example.fixpricedemo.domen.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Category(
    val id: Long,
    val title: String,
    val catalogImage: String
): Parcelable
