package com.example.fixpricedemo.domen.repositories

import com.example.fixpricedemo.domen.model.Category

interface CategoryRepository {

    suspend fun getCategory(): List<Category>
}