package com.example.fixpricedemo.ui.screens.ui.theme

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.fixpricedemo.R
import com.example.fixpricedemo.ui.screens.CategoryCard
import com.example.fixpricedemo.ui.screens.CategoryViewModel
import com.example.fixpricedemo.ui.theme.greenPrimary
import org.koin.androidx.compose.koinViewModel
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryScreen(viewModel: CategoryViewModel = koinViewModel()) {

    val state = viewModel.collectAsState()
    viewModel.collectSideEffect {}
    Column {
        TopAppBar(title = {   Row {
            Text(
                text = stringResource(id = R.string.app_bar_category_title),
                color = colorResource(id = android.R.color.white)
            )
        } },
            colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = greenPrimary))
        LazyVerticalGrid(columns = GridCells.Fixed(2),
            content = {
                items(state.value.categories.size) { index ->
                    CategoryCard(state.value.categories[index])
                }
            })
    }
}

