package com.example.fixpricedemo.ui.screens

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.fixpricedemo.domen.repositories.CategoryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container

class CategoryViewModel(
    savedStateHandle: SavedStateHandle,
    private val categoryRepository: CategoryRepository
) : ContainerHost<CategoryState, CategorySideEffect>, ViewModel() {

    override val container =
        container<CategoryState, CategorySideEffect>(CategoryState(), savedStateHandle) {
            getCategory()
        }

    private fun getCategory() {
        intent {
            viewModelScope.launch(Dispatchers.IO) {
                val categories = categoryRepository.getCategory()
                reduce {
                    state.copy(
                        categories = categories
                    )
                }
            }
        }
    }
}