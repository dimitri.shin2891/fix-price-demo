package com.example.fixpricedemo.ui.screens

import android.os.Parcelable
import com.example.fixpricedemo.domen.model.Category
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryState(
    val categories: List<Category> = emptyList()
) : Parcelable