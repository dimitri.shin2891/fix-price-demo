# Fix Price Demo

Выполнил:

Шин Дмитрий Олегович

dimitri.shin2891@gmail.com - почта

@dmitriy289 - телеграмм


Тестовое задание для демонстрации понимания компонентов Android

Реализована минимальная продуктовая фича получение категорий товаров. Для получения данных используется API_URL = "https://a-api.fix-price.com"
урл для получения категорий "/buyer/v2/category" параметр "cityid" "захардкожен"

Используемые компоненты и подходы для реализации:

- Clean Architecture
- MVI
- Orbit
- Coroutines
- Compose
- Koin
- Сoil

